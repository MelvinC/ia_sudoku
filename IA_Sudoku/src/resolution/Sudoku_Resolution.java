package resolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Stack;
import sudoku.Case;
import sudoku.Pair;
import sudoku.Sudoku;

public class Sudoku_Resolution {

	private Sudoku sudoku;

	public Sudoku_Resolution(Sudoku s) {
		this.sudoku = s;
	}

	// Backtracking-Search
	public Sudoku backtracking_search() throws Exception {
		return recursive_backtracking(this.sudoku);
	}

	private Sudoku recursive_backtracking(Sudoku s) throws Exception {
		if (isFull(s)) {
			return s;
		}
		if (s.isEmpty()) {
			s.setCase(1, new Pair<Integer, Integer>(0, 0));
			if (s.getSize() == 1) {
				return s;
			}
		}
		// Choix de la case vide
		Case var = select_unassigned_variable(s);
		s = ac3(s);
		if (s == null) {
			return null;
		}
		for (int v : least_constraining_value(var, s)) {
			if (constraints_check(v, var, s)) {
				var.setValue(v);
				LinkedList<Integer> domain = var.getDomain();
				var.resetDomain();
				s.popFromAllDomain(v, var);
				if (s != null) {
					Sudoku result = recursive_backtracking(s);
					if (result != null)
						return result;
				}
				var.setValue(0); // On enleve la valeur ajoutee qui etait problematique
				var.setDomain(domain);
			}
		}
		return null;
	}

	private boolean isFull(Sudoku s) {
		for (Case[] l : s.getGrille())
			for (Case c : l)
				if (c.getValue() == 0)
					return false;
		return true;
	}

	private Case select_unassigned_variable(Sudoku s) throws Exception {
		return degree_heuristic(mrv(s), s);
	}

	private ArrayList<Case> mrv(Sudoku s) throws Exception {
		ArrayList<Case> res = new ArrayList<Case>();
		ArrayList<Integer> listLines = new ArrayList<Integer>();
		int minPossibilitiesLine = s.getSize() + 1;
		ArrayList<Integer> listColumns = new ArrayList<Integer>();
		int minPossibilitiesColumn = s.getSize() + 1;
		ArrayList<Case> listSquare = new ArrayList<Case>();
		int minPossibilitiesSquare = s.getSize() + 1;
		for (int i = 0; i < s.getSize(); i++) {
			int zeros = s.countZeros(s.getLigne(i));
			if (zeros < minPossibilitiesLine && zeros != 0) {
				minPossibilitiesLine = zeros;
				listLines.clear();
				listLines.add(i);
			}
			if (zeros == minPossibilitiesLine) {
				listLines.add(i);
			}
		}
		for (int i = 0; i < s.getSize(); i++) {
			int zeros = s.countZeros(s.getColonne(i));
			if (zeros < minPossibilitiesColumn && zeros != 0) {
				minPossibilitiesColumn = zeros;
				listColumns.clear();
				listColumns.add(i);
			}
			if (zeros == minPossibilitiesColumn) {
				listLines.add(i);
			}
		}
		int pas = (int) Math.sqrt(s.getSize());
		for (int i = 0; i < s.getSize(); i += pas) {
			for (int j = 0; j < s.getSize(); j += pas) {
				int zeros = s.countZeros(s.getCarre(i, j));
				if (zeros < minPossibilitiesSquare && zeros != 0) {
					minPossibilitiesSquare = zeros;
					listSquare.clear();
					listSquare.add(s.getCase(i, j));
				}
				if (zeros == minPossibilitiesColumn) {
					listSquare.add(s.getCase(i, j));
				}
			}
		}
		if (minPossibilitiesSquare <= minPossibilitiesLine && minPossibilitiesSquare <= minPossibilitiesColumn) {
			for (Case c : listSquare) {
				res.addAll(getEmptiesInSquare(c, s));
			}
		}
		if (minPossibilitiesLine <= minPossibilitiesColumn && minPossibilitiesLine <= minPossibilitiesSquare) {
			for (int line : listLines) {
				res.addAll(getEmptiesInLine(s.getLigne(line), line));
			}
		}
		if (minPossibilitiesColumn <= minPossibilitiesLine && minPossibilitiesColumn <= minPossibilitiesSquare) {
			for (int column : listColumns) {
				res.addAll(getEmptiesInColumn(s.getColonne(column), column));
			}
		}
		return res;
	}

	public ArrayList<Case> getEmptiesInLine(Case[] tab, int line) {
		ArrayList<Case> res = new ArrayList<Case>();
		for (int i = 0; i < tab.length; i++) {
			if (tab[i].getValue() == 0) {
				res.add(tab[i]);
			}
		}
		return res;
	}

	public ArrayList<Case> getEmptiesInColumn(Case[] tab, int column) {
		ArrayList<Case> res = new ArrayList<Case>();
		for (int i = 0; i < tab.length; i++) {
			if (tab[i].getValue() == 0) {
				res.add(tab[i]);
			}
		}
		return res;
	}

	public ArrayList<Case> getEmptiesInSquare(Case c, Sudoku s) throws Exception {
		ArrayList<Case> res = new ArrayList<Case>();
		for (Case _case : s.getCarre(c.getPair().getX(), c.getPair().getY())) {
			if (_case.getValue() == 0) {
				res.add(_case);
			}
		}
		return res;
	}

	private Case degree_heuristic(ArrayList<Case> list, Sudoku s) throws Exception {
		Case res = null;
		int maxConstraints = s.getSize() + 1;
		for (Case c : list) {
			if (c.getDomain().size() < maxConstraints) {
				res = c;
				maxConstraints = c.getDomain().size();
			}
		}
		return res;
	}

	private ArrayList<Integer> least_constraining_value(Case c, Sudoku s) throws Exception {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < s.getSize(); i++) {
			res.add(0);
		}
		for (int l : Sudoku.ORDER_DOMAIN_VALUE) {
			Case _case = s.getCase(l - 1, c.getPair().getY());
			if (_case.getValue() != 0) {
				for (int v : _case.getDomain()) {
					res.set(v - 1, res.get(v - 1) + 1);
				}
			}
		}
		for (int column : Sudoku.ORDER_DOMAIN_VALUE) {
			Case _case = s.getCase(c.getPair().getX(), column - 1);
			if (_case.getValue() != 0) {
				for (int v : _case.getDomain()) {
					res.set(v - 1, res.get(v - 1) + 1);
				}
			}
		}
		for (Case _case : s.getCarre(c.getPair().getX(), c.getPair().getY())) {
			if (_case.getValue() != 0) {
				for (int v : _case.getDomain()) {
					res.set(v - 1, res.get(v - 1) + 1);
				}
			}
		}
		return sortIndexByValue(res);
	}

	private ArrayList<Integer> sortIndexByValue(ArrayList<Integer> tab) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < this.sudoku.getSize(); i++) {
			int index = tab.indexOf(Collections.min(tab));
			res.add(index + 1);
			tab.set(index, Integer.MAX_VALUE);
		}
		return res;
	}

	private Sudoku ac3(Sudoku s) throws Exception {
		Stack<Pair<Case, Case>> stack = new Stack<Pair<Case, Case>>();
		for (Case[] line : s.getGrille()) {
			for (Case c : line) {
				if (c.getValue() == 0) {
					stack = addArcs(c, s, stack);
				}
			}
		}
		while (!stack.isEmpty()) {
			Pair<Case, Case> p = stack.pop();
			int bool = remove_inconsistent_value(p);
			if (bool == 1) {
				stack = addNeighborsArcs(p.getY(), s, stack);
			} else if (bool == 2) {
				return null;
			}
		}
		return s;
	}

	private Stack<Pair<Case, Case>> addArcs(Case _case, Sudoku s, Stack<Pair<Case, Case>> stack) throws Exception {
		Pair<Integer, Integer> pair = _case.getPair();
		for (Case c : s.getLigne(pair.getX())) {
			if (!c.equals(_case)) {
				stack.add(new Pair<Case, Case>(_case, c));
			}
		}
		for (Case c : s.getColonne(pair.getY())) {
			if (!c.equals(_case)) {
				stack.add(new Pair<Case, Case>(_case, c));
			}
		}
		for (Case c : s.getCarre(pair.getX(), pair.getY())) {
			if (!c.equals(_case)) {
				Pair<Case, Case> p = new Pair<Case, Case>(_case, c);
				if (!contains(stack, p)) {
					stack.add(p);
				}
			}
		}
		return stack;
	}

	private Stack<Pair<Case, Case>> addNeighborsArcs(Case _case, Sudoku s, Stack<Pair<Case, Case>> stack)
			throws Exception {
		Pair<Integer, Integer> pair = _case.getPair();
		for (Case c : s.getLigne(pair.getX())) {
			if (!c.equals(_case) && c.getValue() == 0) {
				Pair<Case, Case> p = new Pair<Case, Case>(c, _case);
				if (!contains(stack, p)) {
					stack.add(p);
				}
			}
		}
		for (Case c : s.getColonne(pair.getY())) {
			if (!c.equals(_case) && c.getValue() == 0) {
				Pair<Case, Case> p = new Pair<Case, Case>(c, _case);
				if (!contains(stack, p)) {
					stack.add(p);
				}
			}
		}
		for (Case c : s.getCarre(pair.getX(), pair.getY())) {
			if (!c.equals(_case) && c.getValue() == 0) {
				Pair<Case, Case> p = new Pair<Case, Case>(c, _case);
				if (!contains(stack, p)) {
					stack.add(p);
				}
			}
		}
		return stack;
	}

	private int remove_inconsistent_value(Pair<Case, Case> pair) {
		for (int x : pair.getX().getDomain()) {
			if (pair.getY().getDomain().size() == 1) {
				if (pair.getX().getDomain().size() == 1 && pair.getY().getDomain().get(0) == x) {
					return 2;
				}
				pair.getX().getDomain().remove((Integer) x);
				return 1;
			}
		}
		return 0;
	}

	private boolean constraints_check(int value, Case c, Sudoku s) throws Exception {
		if (!specific_constraints_check(s.getLigne(c.getPair().getX()), value)) {
			return false;
		}
		if (!specific_constraints_check(s.getColonne(c.getPair().getY()), value)) {
			return false;
		}
		if (!specific_constraints_check(s.getCarre(c.getPair().getX(), c.getPair().getY()), value)) {
			return false;
		}
		return true;
	}

	private boolean specific_constraints_check(Case[] values, int value) {
		for (Case i : values) {
			if (i.getValue() == value) {
				return false;
			}
		}
		return true;
	}

	private boolean contains(Stack<Pair<Case, Case>> stack, Pair<Case, Case> pair) {
		for (Pair<Case, Case> p : stack) {
			if (p.equals(pair)) {
				return true;
			}
		}
		return false;
	}
}