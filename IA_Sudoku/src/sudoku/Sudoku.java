package sudoku;

public class Sudoku implements Cloneable {
	public static int[] ORDER_DOMAIN_VALUE = new int[1];

	private Case[][] grille;

	public Sudoku(int[][] grille) throws Exception {
		ORDER_DOMAIN_VALUE = new int[grille.length];
		for (int i = 0; i < grille.length; i++) {
			ORDER_DOMAIN_VALUE[i] = i + 1;
		}
		this.grille = new Case[grille.length][grille.length];
		int line = 0;
		int column = 0;
		for (int[] i : grille) {
			for (int j : i) {
				this.grille[line][column] = new Case(new Pair<Integer, Integer>(line, column), j);
				column++;
			}
			line++;
			column = 0;
		}
		// Mise a jour des domain
		line = 0;
		column = 0;
		for (int[] i : grille) {
			for (int j : i) {
				if (j != 0) {
					popFromAllDomain(j, this.grille[line][column]);
				}
				column++;
			}
			line++;
			column = 0;
		}
		// Verification
//		line = 0;
//		column = 0;
//		for (int[] i : grille) {
//			for (int j : i) {
//				if(j != 0) {
//					StringBuilder sb = new StringBuilder();
//					//for(Case c : getCarre(this.grille[line][column].getPair().getX(), this.grille[line][column].getPair().getY())){
//					for(Case c : getCarre(line, column)){
//						sb.append(c.getValue());
//						sb.append(",");
//					}
//					System.out.println(sb.toString());
//					System.out.println(this.grille[line][column].getDomain());
//				}
//				column++;
//			}
//			line++;
//			column = 0;
//		}
	}

	public Sudoku() {
		for (int i = 0; i < grille.length; i++) {
			ORDER_DOMAIN_VALUE[i] = i + 1;
		}
		this.grille = new Case[grille.length][grille.length];
	}

	public Sudoku cloneSudoku() throws Exception {
		Sudoku res = new Sudoku();
		res.setGrille(this.grille.clone());
		return res;
	}

	public void popFromAllDomain(int v, Case _case) throws Exception {
		for (Case c : getLigne(_case.getPair().getX())) {
			if (c.getValue() != v)
				c.getDomain().remove((Integer) v);
		}
		for (Case c : getColonne(_case.getPair().getY())) {
			if (c.getValue() != v)
				c.getDomain().remove((Integer) v);
		}
		for (Case c : getCarre(_case.getPair().getX(), _case.getPair().getY())) {
			if (c.getValue() != v)
				c.getDomain().remove((Integer) v);
		}
	}

	public int getSize() {
		return grille.length;
	}

	public Case[] getLigne(int n) throws Exception {
		if (n > grille.length - 1) {
			throw new Exception("L'index de la ligne ne peut pas etre superieur a " + (grille.length - 1));
		}
		return grille[n];
	}

	public Case[] getColonne(int n) throws Exception {
		if (n > grille.length - 1) {
			throw new Exception("L'index de la colonne ne peut pas etre superieur a " + (grille.length - 1));
		}
		Case[] colonne = new Case[grille.length];
		for (int i = 0; i < grille.length; i++) {
			colonne[i] = grille[i][n];
		}
		return colonne;
	}

	public Case[] getCarre(int l, int c) throws Exception {
		if (l > grille.length - 1)
			throw new Exception("L'index de la ligne du carre ne peut pas etre superieur a " + (grille.length - 1));
		if (c > grille.length - 1)
			throw new Exception("L'index de la colonne du carre ne peut pas etre superieur a " + (grille.length - 1));
		int ligne = (int) ((l - l % Math.sqrt(grille.length)) / Math.sqrt(grille.length));
		int colonne = (int) ((c - c % Math.sqrt(grille.length)) / Math.sqrt(grille.length));
		Case[] carre = new Case[grille.length];
		for (int i = 0; i < Math.sqrt(grille.length); i++) {
			for (int j = 0; j < Math.sqrt(grille.length); j++)
				carre[(int) (i * Math.sqrt(grille.length) + j)] = grille[(int) (ligne * Math.sqrt(grille.length)
						+ i)][(int) (colonne * Math.sqrt(grille.length) + j)];
		}
		return carre;
	}

	public Pair<Integer, Integer>[] getPairsCarre(int l, int c) throws Exception {
		if (l > grille.length - 1)
			throw new Exception("L'index de la ligne du carre ne peut pas etre superieur a " + (grille.length - 1));
		if (c > grille.length - 1)
			throw new Exception("L'index de la colonne du carre ne peut pas etre superieur a " + (grille.length - 1));
		@SuppressWarnings("unchecked")
		Pair<Integer, Integer>[] res = new Pair[grille.length];
		int ligne = (int) ((l - l % Math.sqrt(grille.length)) / Math.sqrt(grille.length));
		int colonne = (int) ((c - c % Math.sqrt(grille.length)) / Math.sqrt(grille.length));
		int cpt = 0;
		for (int i = 0; i < Math.sqrt(grille.length); i++) {
			for (int j = 0; j < Math.sqrt(grille.length); j++) {
				res[cpt] = new Pair<Integer, Integer>((int) (ligne * Math.sqrt(grille.length) + i),
						(int) (colonne * Math.sqrt(grille.length) + j));
				cpt++;
			}
		}
		return res;
	}

	public Case getCase(int i, int j) {
		return grille[i][j];
	}

	public void setCase(int value, Pair<Integer, Integer> var) {
		this.grille[var.getX()][var.getY()].setValue(value);
	}

	public Case[][] getGrille() {
		return this.grille;
	}

	public void setGrille(Case[][] grille) {
		this.grille = grille;
	}

	public int countZeros(Case[] tab) {
		int res = 0;
		for (Case i : tab) {
			if (i.getValue() == 0) {
				res++;
			}
		}
		return res;
	}

	public boolean isEmpty() {
		for (int i = 0; i < grille.length; i++) {
			for (int j = 0; j < grille.length; j++) {
				if (this.grille[i][j].getValue() != 0) {
					return false;
				}
			}
		}
		return true;
	}

	public void checkForMistakes() throws Exception {
		boolean mistake = false;
		for (int i = 0; i < grille.length; i++) {
			mistake &= containsMistake(getLigne(i)) && containsMistake(getColonne(i));
		}
		for (int i = 0; i < Math.sqrt(grille.length); i++) {
			for (int j = 0; j < Math.sqrt(grille.length); j++) {
				mistake &= containsMistake(
						getCarre((int) (i * Math.sqrt(grille.length)), (int) (j * Math.sqrt(grille.length))));
			}
		}
		if (mistake) {
			System.out.println("Cette grille de sudoku comporte des erreurs !");
		} else {
			System.out.println("Cette grille de sudoku ne comporte pas d'erreurs !");
		}
		System.out.println();
	}

	public boolean containsMistake(Case[] cases) {
		boolean[] test = new boolean[grille.length];
		for (int i = 0; i < grille.length; i++) {
			test[i] = false;
		}
		for (int i = 0; i < grille.length; i++) {
			test[cases[i].getValue() - 1] = true;
		}
		for (int i = 0; i < grille.length; i++) {
			if (!test[i]) {
				return true;
			}
		}
		return false;
	}

	public void print() {
		if (grille.length < 10) {
			for (int i = 0; i < grille.length; i++) {
				for (int j = 0; j < grille.length; j++) {
					if (j % Math.sqrt(grille.length) == 0) {
						System.out.print("|" + (grille[i][j].getValue() == 0 ? " " : grille[i][j].getValue()));
					} else {
						System.out.print((grille[i][j].getValue() == 0 ? "  " : " " + grille[i][j].getValue()));
					}
				}
				System.out.println("|");
				if (i % Math.sqrt(grille.length) == Math.sqrt(grille.length) - 1 && i < grille.length - 1) {
					for (int n = 0; n < grille.length * 2; n++) {
						System.out.print("-");
					}
					System.out.println("-");
				}
			}
			System.out.println();
		} else {
			for (int i = 0; i < grille.length; i++) {
				for (int j = 0; j < grille.length; j++) {
					if (j % Math.sqrt(grille.length) == 0) {
						System.out.print("|" + (grille[i][j].getValue() == 0 ? "  "
								: (grille[i][j].getValue() < 10 ? "0" + grille[i][j].getValue()
										: grille[i][j].getValue())));
					} else {
						System.out.print((grille[i][j].getValue() == 0 ? "   "
								: " " + (grille[i][j].getValue() < 10 ? "0" + grille[i][j].getValue()
										: grille[i][j].getValue())));
					}
				}
				System.out.println("|");
				if (i % Math.sqrt(grille.length) == Math.sqrt(grille.length) - 1 && i < grille.length - 1) {
					for (int n = 0; n < grille.length * 3; n++) {
						System.out.print("-");
					}
					System.out.println("-");
				}
			}
			System.out.println();
		}
	}

	public void printPart(int[] part) {
		for (int i = 0; i < grille.length; i++) {
			System.out.print(part[i]);
		}
		System.out.println();
	}
}