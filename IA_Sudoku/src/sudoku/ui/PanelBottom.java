package sudoku.ui;

import javax.swing.JPanel;

public class PanelBottom extends JPanel {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private FrameSudoku frame;

	public PanelBottom(FrameSudoku frame) {
		Button button = new Button(0, 0, 200, 50, 10, "Selectionner un sudoku", frame);
		super.add(button);
		button = new Button(50, 50, 200, 50, 10, "Resoudre le sudoku", frame);
		super.add(button);
	}
}