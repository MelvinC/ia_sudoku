package sudoku.ui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import sudoku.Sudoku;

public class SudokuReader {
	public static Sudoku readSS() throws Exception {
		JFileChooser jfc = new JFileChooser("./src/Sudokus (fichiers .ss)");
		jfc.setDialogTitle("Selectionnez un sudoku (fichier .ss) a resoudre");
		String suffixe = "";
		String path = "";
		do {
			int ret = jfc.showSaveDialog(jfc);
			if (ret == JFileChooser.APPROVE_OPTION) {
				path = jfc.getSelectedFile().getAbsolutePath();
				if (path.contains(".")) {
					int i = path.lastIndexOf('.');
					if (i > 0 && i < path.length() - 1) {
						suffixe = path.substring(i + 1).toLowerCase();
					}
				}
				if (!suffixe.equals("ss")) {
					JOptionPane.showMessageDialog(null, "L'extension du fichier doit �tre .ss");
				}
			} else {
				// throw new Exception("Vous avez interrompu le programme puisque vous n'avez
				// pas selectionne de sudoku");
				return null;
			}
		} while (!suffixe.equals("ss"));

		String sudoku = "";
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		int size;
		sudoku = sb.toString();
		if (sudoku.contains(",")) {
			sudoku = sudoku.replace(".", "0").replaceAll("-!", "").replaceAll("!", ",").replaceAll("-\n", "")
					.replaceAll("-", "").replaceAll("\n", ",").replaceAll(",", " ");
		} else {
			sudoku = sudoku.replace(".", "0").replaceAll("[^0-9]", "").replaceAll("[0-9]", "$0 ");
		}
		size = (int) Math.sqrt(sudoku.split(" ").length);

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(sudoku);
		int[][] grille = new int[size][size];
		int rows = 0;
		int columns = 0;
		while (scanner.hasNextInt()) {
			grille[columns][rows] = scanner.nextInt();
			rows++;
			if (rows > size - 1) {
				rows = 0;
				columns++;
			}
		}
		return new Sudoku(grille);
	}

	public static void main(String[] args) {
		try {
			Sudoku sudoku = readSS();
			if (sudoku != null) {
				sudoku.print();
				sudoku.getLigne(1);
				sudoku.getColonne(5);
				sudoku.getCarre(0, 2);
				System.out.println(sudoku.getCase(0, 1).getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}