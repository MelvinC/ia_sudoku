package sudoku.ui;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

public class PanelSudoku extends JPanel {
	private static final long serialVersionUID = 1L;

	private FrameSudoku frame;
	private int size;

	public PanelSudoku(FrameSudoku frame, int size) {
		this.frame = frame;
		this.size = size;
	}

	public void setSize(int n) {
		this.size = n;
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(4));
		g2.draw(new Line2D.Float(0, 0, 0, this.getHeight()));
		g2.draw(new Line2D.Float(0, 0, this.getWidth(), 0));
		g2.draw(new Line2D.Float(this.getWidth(), 0, this.getWidth(), this.getHeight()));
		g2.draw(new Line2D.Float(0, this.getHeight(), this.getWidth(), this.getHeight()));

		g2.setStroke(new BasicStroke(2));
		for (int i = 1; i < Math.sqrt(size); i++) {
			g2.draw(new Line2D.Float(i * this.getWidth() / (float) Math.sqrt(size), 0,
					i * this.getWidth() / (float) Math.sqrt(size), this.getHeight()));
			g2.draw(new Line2D.Float(0, i * this.getHeight() / (float) Math.sqrt(size), this.getWidth(),
					i * this.getHeight() / (float) Math.sqrt(size)));
		}

		g2.setStroke(new BasicStroke(1));
		for (int i = 1; i < size; i++) {
			if (i % Math.sqrt(size) != 0) {
				g2.draw(new Line2D.Float(i * this.getWidth() / size, 0, i * this.getWidth() / size, this.getHeight()));
				g2.draw(new Line2D.Float(0, i * this.getHeight() / size, this.getWidth(), i * this.getHeight() / size));
			}
		}

		for (int j = 0; j < size; j++) {
			for (int i = 0; i < size; i++) {
				g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 25));
				drawStringCentered(g, this.getWidth() / (size * 2) + i * this.getWidth() / size,
						this.getHeight() / (size * 2) + j * this.getHeight() / size,
						"" + (this.frame.getSudoku().getCase(j, i).getValue() == 0 ? ""
								: this.frame.getSudoku().getCase(j, i).getValue()));
			}
		}
	}

	public void drawStringCentered(Graphics g, int x, int y, String text) {
		g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 300 / this.size));
		Rectangle2D r2D = g.getFont().getStringBounds(text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = x - (rW / 2) - rX;
		int b = y - (rH / 2) - rY;
		g.drawString(text, a, b);
	}
}