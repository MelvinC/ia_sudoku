package sudoku.ui;

import java.awt.Color;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import resolution.Sudoku_Resolution;
import sudoku.Sudoku;

public class FrameSudoku extends JFrame {
	private static final long serialVersionUID = 1L;

	private final static int frameWidth = 920;
	private final static int frameHeight = 975;

	private JFrame frame;
	private JPanel panel;
	private PanelSudoku panelSudoku;
	private PanelBottom panelBottom;
	private Sudoku sudoku;

	public static void main(String s[]) throws Exception {
		FrameSudoku frame = new FrameSudoku();
		int[][] grille = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				grille[i][j] = 0;
			}
		}
		frame.sudoku = new Sudoku(grille);
		frame.doGraphics();
	}

	public void setSudoku(Sudoku sudoku) {
		if (sudoku != null) {
			this.sudoku = sudoku;
			this.panelSudoku.setSize(sudoku.getSize());
			this.frame.repaint();
		}
	}

	public Sudoku getSudoku() {
		return this.sudoku;
	}

	public void getRes() throws Exception {
		this.sudoku.print();
		Sudoku_Resolution res = new Sudoku_Resolution(this.sudoku);
		this.setSudoku(res.backtracking_search());
		this.sudoku.print();
		this.sudoku.checkForMistakes();
	}

	public void doGraphics() {
		this.frame = new JFrame("IA Sudoku");
		this.panel = new JPanel();
		this.panel.setBackground(Color.WHITE);
		this.panel.setLayout(null);

		this.panelSudoku = new PanelSudoku(this, this.sudoku.getSize());
		this.panelSudoku.setBackground(Color.WHITE);
		this.panelSudoku.setBounds(50, 25, 800, 800);
		this.panel.add(this.panelSudoku);

		this.panelBottom = new PanelBottom(this);
		this.panelBottom.setBackground(Color.WHITE);
		this.panelBottom.setBounds(50, 850, 800, 75);
		this.panel.add(this.panelBottom);

		this.frame.add(this.panel);
		this.frame.addComponentListener(new FrameAdapter(this.frame));
		this.frame.getContentPane();
		this.frame.setSize(FrameSudoku.frameWidth, FrameSudoku.frameHeight);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class FrameAdapter extends ComponentAdapter {
		private JFrame frame;

		public FrameAdapter(JFrame frame) {
			this.frame = frame;
		}

		@Override
		public void componentResized(ComponentEvent e) {
			this.frame.repaint();
		}
	}
}