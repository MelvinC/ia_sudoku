package sudoku;

public class Pair<E, F> {
	E x;
	F y;

	public Pair(E x, F y) {
		this.x = x;
		this.y = y;
	}

	public E getX() {
		return this.x;
	}

	public F getY() {
		return this.y;
	}

	public boolean equals(Pair<E, F> p) {
		return this.x.equals(p.getX()) && this.y.equals(p.getY());
	}

	public String toString() {
		return x + " & " + y;
	}
}