package sudoku;

import java.util.LinkedList;

public class Case {
	private int value;
	private Pair<Integer, Integer> pair;
	private LinkedList<Integer> domain;

	public Case(Pair<Integer, Integer> p, int value) {
		this.value = value;
		this.pair = p;
		this.domain = new LinkedList<Integer>();
		if (value != 0) {
			this.domain.add(value);
		} else {
			for (int i : Sudoku.ORDER_DOMAIN_VALUE) {
				this.domain.add(i);
			}
		}
	}

	public int getValue() {
		return this.value;
	}

	public void setValue(int v) {
		this.value = v;
	}

	public Pair<Integer, Integer> getPair() {
		return this.pair;
	}

	public LinkedList<Integer> getDomain() {
		return this.domain;
	}

	public void setDomain(LinkedList<Integer> domain) {
		this.domain = domain;
	}

	public void resetDomain() {
		this.domain.clear();
		this.domain.add(this.value);
	}

	public void removeFromDomain(int x) {
		if (x != this.value)
			this.domain.remove((Integer) x);
	}

	public boolean equals(Case c) {
		return (c.getPair().getX().equals(this.pair.getX()) && c.getPair().getY().equals(this.pair.getY()));
	}

	public String toString() {
		return pair.toString();
	}
}